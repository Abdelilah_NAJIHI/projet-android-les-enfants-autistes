package com.example.timeractivity

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.fonts.Font
import android.os.Bundle
import android.os.CountDownTimer

import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.timeractivity.PrefUtil

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
//import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.res.ResourcesCompat
import kotlinx.android.synthetic.main.settings_activity.*
import java.time.Period
import java.util.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit


class MainActivity : AppCompatActivity() {
    enum class TimerState{
        Stopped, Paused, Running
    }

    private lateinit var timer: CountDownTimer
    private var timerLengthSeconds: Long = 0
    private var timerState = TimerState.Stopped
   // private val dateD = findViewById<TextView>(R.id.etTime1)

    private var secondsRemaining: Long = 0

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
       //Sound

        //var mp = MediaPlayer.create(applicationContext,R.raw.sons)
       // val date1 = LocalDateTime.parse(dateD.toString())
        val c =Calendar.getInstance()
        val heure =c.get(Calendar.HOUR_OF_DAY)
        val minute =c.get(Calendar.MINUTE)
        etTime1.setText(""+heure + ":" + minute)
        setSupportActionBar(toolbar)
        supportActionBar?.setIcon(R.drawable.ic_timer)
        supportActionBar?.title = "      Timer"

        setSupportActionBar(toolbar)
        supportActionBar?.setIcon(R.drawable.ic_timer)
        supportActionBar?.title = "      Timer"

        fab_start.setOnClickListener{v ->
            startTimer()
            timerState =  TimerState.Running
            updateButtons()
            var min = etTime2.text.toString().toInt()
            var i = Intent(applicationContext, MyBroadcastReceiver::class.java)
            var pi = PendingIntent.getBroadcast(applicationContext, 111, i, 0)
            var am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            am.set(AlarmManager.RTC_WAKEUP,System.currentTimeMillis() + (min*60000),pi)
            Toast.makeText(applicationContext, "Alarm is set fo $min minutes",Toast.LENGTH_LONG).show()
        }

        fab_pause.setOnClickListener { v ->
            timer.cancel()
            timerState = TimerState.Paused
            updateButtons()
        }

        fab_stop.setOnClickListener { v ->
            timer.cancel()
            onTimerFinished()

        }


    }





    override fun onResume() {
        super.onResume()

        initTimer()


    }

    override fun onPause() {
        super.onPause()

        if (timerState == TimerState.Running){
            timer.cancel()

        }
        else if (timerState == TimerState.Paused){

        }

        PrefUtil.setPreviousTimerLengthSeconds(timerLengthSeconds, this)
        PrefUtil.setSecondsRemaining(secondsRemaining, this)
        PrefUtil.setTimerState(timerState, this)
    }

    private fun initTimer(){
        timerState = PrefUtil.getTimerState(this)

        //we don't want to change the length of the timer which is already running
        //if the length was changed in settings while it was backgrounded
        if (timerState == TimerState.Stopped)
            setNewTimerLength()
        else
            setPreviousTimerLength()

        secondsRemaining = if (timerState == TimerState.Running || timerState == TimerState.Paused)
            PrefUtil.getSecondsRemaining(this)
        else
            timerLengthSeconds



        //resume where we left off
        if (timerState == TimerState.Running)
            startTimer()

        updateButtons()
        updateCountdownUI()
    }

    private fun onTimerFinished(){
        timerState = TimerState.Stopped

        //set the length of the timer to be the one set in SettingsActivity
        //if the length was changed when the timer was running
        setNewTimerLength()

        progress_countdown.progress = 0

        PrefUtil.setSecondsRemaining(timerLengthSeconds, this)
        secondsRemaining = timerLengthSeconds

        updateButtons()
        updateCountdownUI()

    }

    private fun startTimer(){
        timerState = TimerState.Running

        timer = object : CountDownTimer(secondsRemaining * 1000, 1000) {
            override fun onFinish() = onTimerFinished()

            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / 1000
                updateCountdownUI()
            }
        }.start()
    }
/*

    val dateF = findViewById<EditText>(R.id.etTime2)
@RequiresApi(Build.VERSION_CODES.O)
fun getDate(){

    val date1 = LocalDateTime.parse(dateD.toString())
    val date2 = LocalDateTime.parse(dateF.toString())
    println(ChronoUnit.DAYS.between(date1, date2))
    //val d: Long = Duration.between(date1, date2).toMinutes()
    // println(date1.until(date2, ChronoUnit.DAYS))
    Log.d("TIME", d.toString())

}
*/


    private fun setNewTimerLength(){

        val lengthInMinutes = PrefUtil.getTimerLength(this)
        timerLengthSeconds = (lengthInMinutes * 60L)
        progress_countdown.max = timerLengthSeconds.toInt()
    }

    private fun setPreviousTimerLength(){
        timerLengthSeconds = PrefUtil.getPreviousTimerLengthSeconds(this)
        progress_countdown.max = timerLengthSeconds.toInt()
    }

    private fun updateCountdownUI(){
        val minutesUntilFinished = secondsRemaining / 60
        val secondsInMinuteUntilFinished = secondsRemaining - minutesUntilFinished * 60
        val secondsStr = secondsInMinuteUntilFinished.toString()
        textView_countdown.text = "$minutesUntilFinished:${if (secondsStr.length == 2) secondsStr else "0" + secondsStr}"
        progress_countdown.progress = (timerLengthSeconds - secondsRemaining).toInt()
    }

    private fun updateButtons(){
        when (timerState) {
            TimerState.Running ->{
                fab_start.isEnabled = false
                fab_pause.isEnabled = true
                fab_stop.isEnabled = true
            }
            TimerState.Stopped -> {
                fab_start.isEnabled = true
                fab_pause.isEnabled = false
                fab_stop.isEnabled = false
            }
            TimerState.Paused -> {
                fab_start.isEnabled = true
                fab_pause.isEnabled = false
                fab_stop.isEnabled = true
            }
        }
    }

val colors = arrayListOf(Color.GRAY,Color.WHITE,Color.RED,Color.rgb(255,0,100),Color.CYAN,Color.rgb(255,0,255))

    fun getRandom (arraysize:Int):Int{
        var randomNum = Random().nextInt(arraysize)
        return randomNum

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.

        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item?.itemId){
            R.id.couleur -> {
                Toast.makeText(this,"la couleur est changée" ,Toast.LENGTH_LONG).show()
                score.setBackgroundColor(colors[getRandom(colors.size)])

                return true

            }
            R.id.visuel -> {

                Toast.makeText(this, "le visuel est changé",Toast.LENGTH_LONG).show()
                var myfont = ResourcesCompat.getFont(this,R.font.alfa_slab_one)
                textView_countdown.typeface = myfont
                return true

            }
            R.id.retour -> {
                finish()
                Toast.makeText(this, "La Page d'Entrée",Toast.LENGTH_LONG).show()
                return true

            }
            R.id.down -> {
                Toast.makeText(this, "Votre Rendez-vous est enregistré",Toast.LENGTH_LONG).show()
                return true

            }
        }
        return super.onOptionsItemSelected(item)
    }

}